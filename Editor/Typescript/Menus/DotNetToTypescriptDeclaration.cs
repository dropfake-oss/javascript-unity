using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Javascript.Codegen.Dts;
using System.IO;

namespace Javascript.Unity
{
	public delegate IReadOnlyList<System.Reflection.Assembly> CodeGenAssembliesDelegate();

	public class DotNetToTypescriptDeclaration
	{
		public static string typescriptDir = "Assets/Generated/Typings";

		public static CodeGenAssembliesDelegate CodeGenAssembliesCallback;

		private static List<Type> _registerTypes = new List<Type>();
		public static IReadOnlyList<Type> RegisteredTypes
		{
			get { return _registerTypes; }
		}

		public static void RegisterType(Type type)
		{
			_registerTypes.Add(type);
		}

		public class CodeGenLogger : ICodeGenLogger
		{
			public void Info(string format, params object[] args)
			{
				if ((args == null) || (args.Length == 0))
				{
					UnityEngine.Debug.Log(format);
				}
				else
				{
					UnityEngine.Debug.LogFormat(format, args);
				}
			}

			public void Debug(string format, params object[] args)
			{
				if ((args == null) || (args.Length == 0))
				{
					UnityEngine.Debug.Log(format);
				}
				else
				{
					UnityEngine.Debug.LogFormat(format, args);
				}
			}

			public void Warn(string format, params object[] args)
			{
				if ((args == null) || (args.Length == 0))
				{
					UnityEngine.Debug.LogWarning(format);
				}
				else
				{
					UnityEngine.Debug.LogWarningFormat(format, args);
				}
			}

			public void Error(string format, params object[] args)
			{
				if ((args == null) || (args.Length == 0))
				{
					UnityEngine.Debug.LogError(format);
				}
				else
				{
					UnityEngine.Debug.LogErrorFormat(format, args);
				}
			}
		}

		private static void WriteAllText(string path, string contents)
		{
			File.WriteAllText(path, contents);
		}

		private static void PopulateUnityTypesForTranslation(TypeTranslator typeTranslator)
		{
			typeTranslator.AddTSTypeNameMap(typeof(LayerMask), "UnityEngine.LayerMask", "number");
			typeTranslator.AddTSTypeNameMap(typeof(Color), "UnityEngine.Color");
			typeTranslator.AddTSTypeNameMap(typeof(Color32), "UnityEngine.Color32");
			typeTranslator.AddTSTypeNameMap(typeof(Vector2), "UnityEngine.Vector2");
			typeTranslator.AddTSTypeNameMap(typeof(Vector2Int), "UnityEngine.Vector2Int");
			typeTranslator.AddTSTypeNameMap(typeof(Vector3), "UnityEngine.Vector3");
			typeTranslator.AddTSTypeNameMap(typeof(Vector3Int), "UnityEngine.Vector3Int");
			typeTranslator.AddTSTypeNameMap(typeof(Vector4), "UnityEngine.Vector4");
			typeTranslator.AddTSTypeNameMap(typeof(Quaternion), "UnityEngine.Quaternion");
		}

		private static void AddExportedTypes(TypescriptDefinitionGeneration typescriptDefinitionGeneration)
		{
			typescriptDefinitionGeneration.RegisterType(typeof(Vector3));
			typescriptDefinitionGeneration.RegisterType(typeof(UnityEngine.Object));

			foreach(var type in RegisteredTypes)
			{
				//UnityEngine.Debug.Log("AddExportedTypes: registering user provided type: " + type);
				typescriptDefinitionGeneration.RegisterType(type);
			}
		}

		private static void WriteTypescriptDeclarationFile(TypescriptDeclarationGenerator generatedType)
		{
			var filename = generatedType.CodeGenType.GetFileName();
			var tsName = filename + ".d.ts";
			var tsPath = Path.Combine(typescriptDir, tsName);

			//Debug.Log("path: " + tsPath);
			//Debug.Log("contents: " + generatedType.ToString());
			WriteAllText(tsPath, generatedType.ToString());
		}

		// TPC: this is not great - we're always giving our code-gen knowledge of these built-in system types so our codegen works with these types
		private static void RegisterSystemAndObservableTypes()
		{
			// TPC: TODO: the following should likely all be registered as part of a config - i.e. if user/caller enables observable/reactive
			RegisterType(typeof(System.IDisposable));
			RegisterType(typeof(System.Exception));
			// IObservable has dependency on IObserver - so we need to codegen IObserver in order to codegen IObservable
			RegisterType(typeof(System.IObserver<>));
			RegisterType(typeof(System.IObservable<>));
		}

		public static void GenerateTypescriptDefinitions(IReadOnlyList<System.Reflection.Assembly> codeGenEntireAssemblies)
		{
			Debug.Log("GenerateTypescriptDefinitions");
			if (!Directory.Exists(typescriptDir))
			{
				Directory.CreateDirectory(typescriptDir);
			}

			CodeGenLogger codegenLogger = new CodeGenLogger();
			TypeTranslator typeTranslator = new TypeTranslator(codegenLogger);
			RegisterSystemAndObservableTypes();
			PopulateUnityTypesForTranslation(typeTranslator);
			TypescriptDefinitionGeneration typescriptDefinitionGeneration = new TypescriptDefinitionGeneration(codegenLogger, typeTranslator, WriteTypescriptDeclarationFile);
			AddExportedTypes(typescriptDefinitionGeneration);

			// We want to add all assemblies to our CodeGen - so it can resolve types that exist external to the assembly of the type we're code-gen'ing (example partial classes or extension methods)
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			List<CodeGenAssembly> codeGenAssemblies = new List<CodeGenAssembly>();
			for (var i = 0; i < assemblies.Length; i++)
			{
				var assembly = assemblies[i];
				//Debug.Log("GenerateTypescriptDefinitions: assembly: " + assembly.FullName + " IsDynamic: " + assembly.IsDynamic);
				if (assembly.IsDynamic)
				{
					continue;
				}
				var codegenEntireAssembly = false;
				foreach (var codegenAssembly in codeGenEntireAssemblies)
				{
					if (codegenAssembly == assembly)
					{
						codegenEntireAssembly = true;
						break;
					}
				}
				codeGenAssemblies.Add(new CodeGenAssembly(assembly, codegenEntireAssembly));
			}

			typescriptDefinitionGeneration.GenerateTypescriptDeclarations(codeGenAssemblies.ToArray());
		}

		[MenuItem("Javascript/Generate d.ts")]
		static void GenerateTypescriptDefinitionsUsingCallbackButton()
		{
			if (CodeGenAssembliesCallback == null)
			{
				Debug.LogError("no callback registered with DotNetToTypescriptDeclaration");
				return;
			}
			var assemblies = CodeGenAssembliesCallback();
			GenerateTypescriptDefinitions(assemblies);
			AssetDatabase.Refresh(); // this forces Unity to reload newly generated files
		}

		[MenuItem("Javascript/Clear Generated Code")]
		static void ClearButton()
		{
			if (!Directory.Exists(typescriptDir))
			{
				Debug.Log("Directory: " + typescriptDir + " does not exist - nothing to clear/delete.");
				return;
			}
			Directory.Delete(typescriptDir, true);
			Debug.Log("Directory: " + typescriptDir + " deleted!");
		}
	}
}
