#if UNITY_2022_2_OR_NEWER

using UnityEngine;
using UnityEditor.AssetImporters;
using System.IO;

/// <summary>
/// Note to others - the following ScriptedImporter is now required for Unity 2021.2.19 to import .js files
/// Earlier versions of Unity will fail with the following error:
/// The scripted importer 'JavascriptFileImporter' attempted to register file type '.js', which is handled by a native Unity importer. Registration rejected.
/// </summary>
[ScriptedImporter(0, new[] { "js", "map" })]
public class JavascriptFileImporter : ScriptedImporter
{
	public override void OnImportAsset(AssetImportContext ctx)
	{
		var asset = new TextAsset(File.ReadAllText(ctx.assetPath));
		ctx.AddObjectToAsset("text", asset);
		ctx.SetMainObject(asset);
	}
}

#endif
