using Javascript.Core;

namespace Javascript.Unity
{
	public class UnityJavascriptLogger : IJavascriptLogger
	{
		public void Debug(string format, params object[] args)
		{
			if (args == null)
			{
				UnityEngine.Debug.Log(format);
			}
			else
			{
				UnityEngine.Debug.LogFormat(format, args);
			}
		}

		public void Error(string format, params object[] args)
		{
			if (args == null)
			{
				UnityEngine.Debug.LogError(format);
			}
			else
			{
				UnityEngine.Debug.LogErrorFormat(format, args);
			}
		}
	}
}

