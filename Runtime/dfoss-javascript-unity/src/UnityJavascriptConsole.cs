using Javascript.Core;

namespace Javascript.Unity
{
	public class UnityJavascriptConsole : IJavascriptConsole
	{
		public static string ConsoleArgsToText(params object[] args)
		{
			// TPC: this is probably not optimal for memory/performance
			System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
			foreach (var obj in args)
			{
				stringBuilder.Append((obj != null) ? obj.ToString() : "null");
				stringBuilder.Append(" "); // by default javascript comma delimited strings auto-add a space
			}
			return stringBuilder.ToString();
		}

		public void log(params object[] args)
		{
			var text = UnityJavascriptConsole.ConsoleArgsToText(args);
			UnityEngine.Debug.Log(text);
		}

		public void warn(params object[] args)
		{
			var text = UnityJavascriptConsole.ConsoleArgsToText(args);
			UnityEngine.Debug.LogWarning(text);
		}

		public void error(params object[] args)
		{
			var text = UnityJavascriptConsole.ConsoleArgsToText(args);
			UnityEngine.Debug.LogError(text);
		}
	}
}

